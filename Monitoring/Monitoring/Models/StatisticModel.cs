﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    class StatisticModel
    {   
            public int Consigment { set; get; }
            public double? AvgDensity { set; get; }
            public string OutputAvgDensity { set; get; }
            public double? AvgMaterial { set; get; }
            public double? AvgDefect { set; get; }
            public double? AvgHallCoefficient { set; get; }
            public string OutputAvgHallCoefficient { set; get; }
            public double? AvgResistivity { set; get; }
            public string OutputAvgResistivity { set; get; }
            public string OutputResistivity { set; get; }
    }
}
