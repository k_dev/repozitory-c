﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring
{
    public class IngotModel
    {
        private Guid idIngot;
        private int numOfConsignment;
        private int numOfIngot;
        private DateTime dateOfGrowth;
        private DateTime dateLabAnalysis;
        private string codOfCriticalSituation1;
        private string codOfCriticalSituation2;
        private string codOfUnforeseenSituation;
        private double percDamage;
        private double minDiameter;
        private double maxDiameter;
        private double resistivity;
        private double hallCoefficient;
        private string outputResistivity;
        private string outputHallCoef;
        private string outputDateOfGrowth;
        private string outputDateOfLabAnalysis;
        public string OutputDateOfGrowth { set; get;}
        public string OutputDateOfLabAnalysis { set; get; }
        public string OutputResistivity { set; get; }
        public string OutputHallCoef { set; get; }
        public Guid IdIngot
        {
            get { return idIngot; }
            set
            {
                if (value != null) idIngot = value;
                else idIngot = Guid.NewGuid();
            }
        }
        public int NumOfConsignment
        {
            get { return numOfConsignment; }
            set
            {
                if (value > 0) numOfConsignment = value;
            }
        }

        public int NumOfIngot
        {
            get { return numOfIngot; }
            set
            {
                if (value > 0) numOfIngot = value;
            }
        }
        public DateTime DateOfGrowth { set; get; }
        public DateTime DateLabAnalysis { set; get; }
        //public double DislocationDensity { set; get; }
        public string CodOfCriticalSituation1
        {
            get { return codOfCriticalSituation1; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) codOfCriticalSituation1 = value;
            }
        }
        public string CodOfCriticalSituation2
        {
            get { return codOfCriticalSituation2; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) codOfCriticalSituation2 = value;
            }
        }

        public string CodOfUnforeseenSituation
        {
            get { return codOfUnforeseenSituation; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) codOfUnforeseenSituation = value;
            }
        }
        public double PercDamage
        {
            get { return percDamage; }
            set
            {
                if ((value > 0)&&(value < 100)) percDamage = value;
                else  percDamage = 0;
            }
        }
        public double MinDiameter
        {
            get { return minDiameter; }
            set
            {
                if (value > 0) minDiameter = value;
                else minDiameter = 0;
            }
        }
        public double MaxDiameter
        {
            get { return maxDiameter; }
            set
            {
                if (value > 0) maxDiameter = value;
                else maxDiameter = 0;
            }
        }
        public double Resistivity
        {
            get { return resistivity; }
            set
            {
                if (value!=0) resistivity = value;
            }
        }

        public double HallCoefficient
        {
            get { return hallCoefficient; }
            set
            {
                if (value!=0) hallCoefficient = value;
            }
        }
    }
}
