﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring
{
  public  class UnforeseenSituationModel
    {
        private string idSituation;
        private string description;
        private string causes;
        private string solution;
      
   
        public string Description
        {
            get { return description; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) description = value;
            }
        }
        public string Causes
        {
            get { return causes; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) causes = value;
            }
        }
        public string Solution
        {
            get { return solution; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) solution = value;
            }
        }
        public string IdSituation
        {
            get { return idSituation; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value)) idSituation = value;
            }
        }

    }
}
