﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Models
{
  public  class PlateModel
    {

        public string OutputDateOfGrowth { set; get; }
        public string OutputDateOfLabAnalysis { set; get; }
        public Guid IdPlate {set; get;}
        public double DislocationDensity { set; get; }
        public int NumPlate { set; get; }
        public int NumIngot { set; get; }
        public DateTime DateOfGrowth { set; get; }
        public DateTime DateOfLabAnalysis { set; get; }
        public int NumOfConsigment { set; get; }
        public string OutputDislocationDensity { set; get; }
        public Guid? IdIngot { set; get; }

    }
}
