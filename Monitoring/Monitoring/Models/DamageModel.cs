﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Models
{
  public  class DamageModel
    {
        private Guid idDamage;
        private Guid idIngot;
        private string idSituation;
        public Guid IdDamage
        {
            get { return idDamage; }
            set
            {
                if (value == null) idDamage = Guid.NewGuid();
                else idDamage = value;
            }
        }

        public Guid IdIngot
        {
            get { return idIngot; }
            set
            {
                if (value != null) idIngot = value;
            }
        }
        public string IdSituation
        {
            get { return idSituation; }
            set
            {
                if (value != null) idSituation = value;
            }
        }
    }
}
