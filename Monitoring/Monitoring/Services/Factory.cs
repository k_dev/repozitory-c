﻿using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Services
{
  public class Factory
    {
      public static IBaseService<IngotModel> CreateIngotService()
      {
          return new IngotService();
      }
      public static IBaseService<UnforeseenSituationModel> CreateUnforeseenSituationService()
      {
          return new UnforeseenSituationService();
      }
      public static IBaseService<DamageModel> CreateDamageService()
      {
          return new DamageService();
      }
      public static IBaseService<PlateModel> CreatePlateService()
      {
          return new PlateService();
      }
    }
}
