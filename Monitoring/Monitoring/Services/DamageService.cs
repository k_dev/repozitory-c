﻿using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Services
{
    class DamageService:IBaseService<DamageModel>
    {
        MonitoringDataEntities context = new MonitoringDataEntities();
        public List<DamageModel> Get()
        {
            throw new NotImplementedException();
        }

        public DamageModel Get(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<DamageModel> Get(int skip, int take)
        {
            throw new NotImplementedException();
        }

        public void Create(DamageModel dataObject)
        {
            var col = context.Damages.Where(c => c.IdIngot == dataObject.IdIngot).Select(c => c.IdSituation);
            if (dataObject.IdSituation.StartsWith("1") && (col.Select(c => c).Where(c => c.StartsWith("1")).Count() >= 1)) throw new Exception();
            if (dataObject.IdSituation.StartsWith("0") && (col.Select(c => c).Where(c => c.StartsWith("0")).Count() >= 2)) throw new Exception();
            Damage obj = new Damage();
            obj.IdDamage = Guid.NewGuid();
            obj.IdSituation = dataObject.IdSituation;
            obj.IdIngot = dataObject.IdIngot;
            context.Damages.Add(obj);
            context.SaveChanges();
        }

        public void Update(DamageModel dataObject)
        {
            throw new NotImplementedException();
        }

        public void Delete(DamageModel dataObject)
        {
            throw new NotImplementedException();
        }
        public List<DamageModel> Search(string str)
        {
            throw new NotImplementedException();
        }
    }
}
