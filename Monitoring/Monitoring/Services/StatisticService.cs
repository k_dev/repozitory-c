﻿using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Services
{
    class StatisticService
    {
        MonitoringDataEntities context = new MonitoringDataEntities();
        public double PartDamaged(List<IngotModel> ingotColl)
        {
            double partDamaged = 0;
            
            foreach (var el in ingotColl)
            {
                int count = 0;
                var plateDisloc = context.Ingots.Where(c=>c.IdIngot==el.IdIngot).Join(context.Plates, i => i.IdIngot, p => p.IdIngot, (i, p) => p.DislocationDensity);
                if (plateDisloc.Count() != 0)
                {
                    foreach (var plateDislVal in plateDisloc)
                    {
                        if (plateDislVal > 70000) count++;
                    }
                }
                if ((el.Resistivity < 1e-04) || (el.Resistivity > 1e+10) || 
                     (el.PercDamage > 65) || (el.MaxDiameter <= el.MinDiameter) || (count == 3)
                     || (el.HallCoefficient < 1e-4) || (el.HallCoefficient > 1e-2) )
                    partDamaged++;
                else if (el.PercDamage != 0 && el.PercDamage <= 65)
                {
                    partDamaged = partDamaged + el.PercDamage/100;
                }
            }
            return partDamaged;
        }
        public Boolean PermissibleDiametr(double diametr)
        {
            if (((diametr >= 39) && (diametr <= 40.5)) || ((diametr >= 50.3) && (diametr <= 61.3)) || ((diametr >= 75.7) && (diametr <= 76.7)))
                return true;
            else return false;
        }

        public void ChangeStatistic(IngotModel dataObject, string action)
        {
            var ingotColl = Factory.CreateIngotService().Get().Where(c => c.NumOfConsignment == dataObject.NumOfConsignment).Select(c => c).ToList();
            if (ingotColl.Count() > 1)
            {
                ConsigmentStatistic item = context.ConsigmentStatistics.Where(c => c.NumConsigment == dataObject.NumOfConsignment).Select(c => c).FirstOrDefault();
                item.AvgResistivity = ingotColl.Select(c => c.Resistivity).Average();
                item.AvgHallKoef = ingotColl.Select(c => c.HallCoefficient).Average();
                var dislocation = context.Ingots.Where(c=>c.NumConsigment==dataObject.NumOfConsignment).Join(context.Plates, i => i.IdIngot, p => p.IdIngot, (i, p) => p.DislocationDensity);
                if (dislocation.Count() == 0)
                    item.AvgDislocDensity = 0;
                else item.AvgDislocDensity = dislocation.Average();
                item.Damage = Math.Round(PartDamaged(ingotColl) / context.Ingots.Where(c => c.NumConsigment == dataObject.NumOfConsignment).Select(c => c).Count() * 100, 2);
                context.SaveChanges();
            }
            else if (ingotColl.Count() ==1&&action!="delete")
            {
                ConsigmentStatistic item = context.ConsigmentStatistics.Where(c => c.NumConsigment == dataObject.NumOfConsignment).Select(c => c).FirstOrDefault();
                item.AvgResistivity = ingotColl.Select(c => c.Resistivity).Average();
                item.AvgHallKoef = ingotColl.Select(c => c.HallCoefficient).Average();
                var dislocation = context.Ingots.Where(c => c.NumConsigment == dataObject.NumOfConsignment).Join(context.Plates, i => i.IdIngot, p => p.IdIngot, (i, p) => p.DislocationDensity);
                if (dislocation.Count() == 0)
                    item.AvgDislocDensity = 0;
                else item.AvgDislocDensity = dislocation.Average();
                item.Damage = Math.Round(PartDamaged(ingotColl) / context.Ingots.Where(c => c.NumConsigment == dataObject.NumOfConsignment).Select(c => c).Count() * 100, 2);
                context.SaveChanges();
            }
         
                if (ingotColl.Count() == 0 && action == "delete") {
                    context.ConsigmentStatistics.Remove(context.ConsigmentStatistics.Where(c => c.NumConsigment == dataObject.NumOfConsignment).FirstOrDefault());
                context.SaveChanges();
            }
        }

        public void ChangeDislocation(Guid idIngot)
        {
            var numConsigment = context.Ingots.Where(c => c.IdIngot == idIngot).Select(c => c.NumConsigment).FirstOrDefault();
            var record = context.ConsigmentStatistics.Where(c => c.NumConsigment == numConsigment).Select(c=>c).FirstOrDefault();
            record.AvgDislocDensity = context.Ingots.Where(c => c.NumConsigment == numConsigment).Join(context.Plates, i => i.IdIngot, p => p.IdIngot, (i, p) => p.DislocationDensity).Average();
            context.SaveChanges();
        }

       public List<StatisticModel> GetStatistic()
        {
            var statisticCol = context.ConsigmentStatistics.Select(c => c);
            List<StatisticModel> list = new List<StatisticModel>();
            if (statisticCol.Count() > 0)
            {
                foreach (var item in statisticCol)
                {
                    StatisticModel obj = new StatisticModel();
                    obj.AvgDefect = Math.Round((double)item.Damage, 2);
                    obj.AvgDensity = item.AvgDislocDensity;
                    obj.AvgHallCoefficient = item.AvgHallKoef;
                    obj.AvgResistivity = item.AvgResistivity;
                    var disloc = (double)item.AvgDislocDensity;
                    obj.OutputAvgDensity = disloc.ToString("E");
                    var hallKoef = (double)item.AvgHallKoef;
                    obj.OutputAvgHallCoefficient = hallKoef.ToString("E");
                    var resistivity = (double)item.AvgResistivity;
                    obj.OutputAvgResistivity = resistivity.ToString("E");
                    obj.Consigment = item.NumConsigment;

                    list.Add(obj);
                }
            }
            return list;
        }

       public List<StatisticModel> Search(string str)
       {
           var searchCol = GetStatistic().Where(c => c.OutputAvgDensity.Contains(str) || c.OutputAvgHallCoefficient.Contains(str)
               || c.OutputAvgResistivity.Contains(str) || c.Consigment.ToString().Contains(str)).Select(c => c);
           return searchCol.ToList();
       }
    }
}
