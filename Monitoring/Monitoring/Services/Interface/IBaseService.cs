﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring
{
  public  interface IBaseService<T>
    {
        List<T> Get();

        T Get(Guid id);

        List<T> Get(int skip, int take);

        void Create(T dataObject);

        void Update(T dataObject);

        void Delete(T dataObject);

        List<T> Search(string str);

    }
}
