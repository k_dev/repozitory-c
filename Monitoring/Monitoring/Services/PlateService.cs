﻿using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Services
{
  public  class PlateService: IBaseService<PlateModel>
    {
        MonitoringDataEntities context = new MonitoringDataEntities();
        StatisticService statistic = new StatisticService();
        public List<PlateModel> Get()
        {
            var collection = context.Plates.Join(context.Ingots, p => p.IdIngot, i => i.IdIngot, (p, i) =>
                new { p.IdIngot, p.IdPlate, p.DislocationDensity, p.NumPlate, i.NumConsigment, i.NumOfIngot, i.DateLabAnalysis, i.DateOfGrowth });
            List<PlateModel> list = new List<PlateModel>();
            foreach(var item in collection)
            {
                PlateModel obj = new PlateModel();
                obj.IdPlate = item.IdPlate;
                obj.DislocationDensity = item.DislocationDensity;
                obj.IdIngot = item.IdIngot;
                obj.NumPlate = item.NumPlate;
                obj.NumOfConsigment = item.NumConsigment;
                obj.NumIngot = item.NumOfIngot;
                obj.DateOfGrowth = item.DateOfGrowth;
                obj.DateOfLabAnalysis = item.DateLabAnalysis;
                obj.OutputDislocationDensity = item.DislocationDensity.ToString("E");
                string[] dateGrowth = item.DateOfGrowth.ToString().Split(' ');
                string[] dateLabAnalysis = item.DateLabAnalysis.ToString().Split(' ');
                obj.OutputDateOfGrowth = dateGrowth[0];
                obj.OutputDateOfLabAnalysis = dateLabAnalysis[0];
                list.Add(obj);
            }
            return list.OrderBy(c=>c.NumOfConsigment).ToList();
        }

        public PlateModel Get(Guid id)
        {
            var item = context.Plates.Where(c=>c.IdPlate==id).Join(context.Ingots, p => p.IdIngot, i => i.IdIngot, (p, i) =>
                new { p.IdIngot, p.IdPlate, p.DislocationDensity, p.NumPlate, i.NumConsigment, i.NumOfIngot, i.DateLabAnalysis, i.DateOfGrowth }).FirstOrDefault();
            PlateModel obj = new PlateModel();
                obj.IdPlate = item.IdPlate;
                obj.DislocationDensity = item.DislocationDensity;
                obj.IdIngot = item.IdIngot;
                obj.NumPlate = item.NumPlate;
                obj.NumOfConsigment = item.NumConsigment;
                obj.NumIngot = item.NumOfIngot;
                obj.DateOfGrowth = item.DateOfGrowth;
                obj.DateOfLabAnalysis = item.DateLabAnalysis;
                return obj;
        }

        public List<PlateModel> Get(int skip, int take)
        {
            return Get().Skip(skip).Take(take).ToList();
        }

        public void Create(PlateModel dataObject)
        {
            Plate obj = new Plate();
            obj.IdPlate = Guid.NewGuid();
            obj.IdIngot = dataObject.IdIngot;
            obj.NumPlate = dataObject.NumPlate;
            obj.DislocationDensity = dataObject.DislocationDensity;
            context.Plates.Add(obj);
            context.SaveChanges();        
            statistic.ChangeDislocation((Guid)dataObject.IdIngot);
        }

        public void Update(PlateModel dataObject)
        {
            var obj = context.Plates.Where(c=>c.IdPlate==dataObject.IdPlate).Select(c=>c).FirstOrDefault();
            obj.IdIngot = dataObject.IdIngot;
            obj.NumPlate = dataObject.NumPlate;
            obj.DislocationDensity = dataObject.DislocationDensity;
            context.SaveChanges();
            statistic.ChangeDislocation((Guid)dataObject.IdIngot);
        }

        public void Delete(PlateModel dataObject)
        {
            var obj = context.Plates.Where(c => c.IdPlate == dataObject.IdPlate).Select(c => c).FirstOrDefault();
            context.Plates.Remove(obj);
            context.SaveChanges();
            statistic.ChangeDislocation((Guid)dataObject.IdIngot);
        }

        public List<PlateModel> Search(string str)
        {
            var col = Get().Where(c => c.OutputDateOfGrowth.Contains(str) || c.OutputDateOfLabAnalysis.Contains(str) ||
                c.DislocationDensity.ToString().Contains(str) || c.NumPlate.ToString().Contains(str) ||
                c.NumIngot.ToString().Contains(str) || c.NumOfConsigment.ToString().Contains(str)).Select(c => c);
            return col.ToList();
        }

      public Guid GetIdOfIngot(int numOfConsigment, int numOfIngot)
        {
            var item = context.Ingots.Where(c => c.NumConsigment == numOfConsigment && c.NumOfIngot == numOfIngot).Select(c => c.IdIngot).FirstOrDefault();
            if (item != Guid.Empty) return item;
            else return Guid.Empty;
        }
    }
}
