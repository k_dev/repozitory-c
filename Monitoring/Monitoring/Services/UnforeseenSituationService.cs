﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring.Services
{
    class UnforeseenSituationService:IBaseService<UnforeseenSituationModel>
    {
            MonitoringDataEntities context = new MonitoringDataEntities();
            public List<UnforeseenSituationModel> Get()
            {
                var collection = context.UnforeseenSituations.Select(c => c);
                List<UnforeseenSituationModel> objs = new List<UnforeseenSituationModel>();
                foreach (var item in collection)
                {
                    UnforeseenSituationModel obj = new UnforeseenSituationModel();
                    obj.IdSituation = item.IdSituation;
                    obj.Causes = item.Causes;
                    obj.Description = item.Description;
                    objs.Add(obj);
                }
                return objs;
            }

            public UnforeseenSituationModel Get(Guid id)
            {
                throw new NotImplementedException();
            }
            public List<UnforeseenSituationModel> Get(int skip, int take)
            {
                throw new NotImplementedException();
            }

            public void Create(UnforeseenSituationModel dataObject)
            {
                 try
                {
                    UnforeseenSituation obj = new UnforeseenSituation();
                    obj.IdSituation = dataObject.IdSituation;
                    obj.Description = dataObject.Description;
                    obj.Causes = dataObject.Causes;
                    context.UnforeseenSituations.Add(obj);
                    context.SaveChanges();
                }
                 catch (Exception) { };
            }

            public void Update(UnforeseenSituationModel dataObject)
            {

                try
                {
                    var obj = context.UnforeseenSituations.Where(c => c.IdSituation == dataObject.IdSituation).Select(c => c).SingleOrDefault();
                    obj.IdSituation = dataObject.IdSituation;
                    obj.Description = dataObject.Description;
                    obj.Causes = dataObject.Causes;
                }
                catch(Exception){};

                context.SaveChanges();
            }

            public void Delete(UnforeseenSituationModel dataObject)
            {
                var obj = context.UnforeseenSituations.Where(c => c.IdSituation == dataObject.IdSituation).Select(c => c).SingleOrDefault();
                context.UnforeseenSituations.Remove(obj);
                context.SaveChanges();
            }

            public List<UnforeseenSituationModel> Search(string str)
            {
                var col = Get().Where(c => c.Causes.Contains(str) || c.Description.Contains(str) ||
                   c.IdSituation.Contains(str)).Select(c => c);
                return col.ToList();
            }
    }
}
