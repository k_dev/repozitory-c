﻿using Monitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring
{
    public class IngotService : IBaseService<IngotModel>
    {
        MonitoringDataEntities context = new MonitoringDataEntities();
        StatisticService statisticObj = new StatisticService();
        public List<IngotModel> Get()
        {
            var collection = context.Ingots.Select(c => c).OrderBy(c => c.NumOfIngot).ToList();
            List<IngotModel> objs = new List<IngotModel>();
            foreach (var item in collection)
            {
                IngotModel obj = new IngotModel();
                obj.IdIngot = item.IdIngot;
                obj.NumOfConsignment = item.NumConsigment;
                obj.NumOfIngot = item.NumOfIngot;
                obj.DateOfGrowth = (DateTime)item.DateOfGrowth;
                obj.DateLabAnalysis = (DateTime)item.DateLabAnalysis;
                obj.PercDamage = item.PercDamage;
                obj.MaxDiameter = item.MaxDiameter;
                obj.MinDiameter = item.MinDiameter;
                obj.Resistivity = item.Resistivity;
                obj.HallCoefficient = item.HallCoefficient;
                obj.OutputHallCoef = item.HallCoefficient.ToString("E");
                obj.OutputResistivity = item.Resistivity.ToString("E");
                    string[] dateGrowth = item.DateOfGrowth.ToString().Split(' ');
                    string[] dateLabAnalysis = item.DateLabAnalysis.ToString().Split(' ');
                    obj.OutputDateOfGrowth = dateGrowth[0];
                    obj.OutputDateOfLabAnalysis = dateLabAnalysis[0];
                var col = context.Ingots.Where(c => c.IdIngot == item.IdIngot).Join(context.Damages, l => l.IdIngot, d => d.IdIngot, (l, d) => d).Select(c => c.IdSituation);

                var code = col.Select(c => c).Where(c => c.StartsWith("1")).ToList();
                if (code.Count() == 1)
                    obj.CodOfUnforeseenSituation = code.FirstOrDefault();
                else obj.CodOfUnforeseenSituation = "-";
                var codeCrit = col.Select(c => c).Where(c => c.StartsWith("0")).ToList();
                if (codeCrit.Count == 1)
                {
                    obj.CodOfCriticalSituation1 = codeCrit.First();
                    obj.CodOfCriticalSituation2 = "-";
                }
                else if (codeCrit.Count == 2)
                {
                    obj.CodOfCriticalSituation1 = codeCrit.First();
                    obj.CodOfCriticalSituation2 = codeCrit.Last();
                }
                else
                {
                    obj.CodOfCriticalSituation1 = "-";
                    obj.CodOfCriticalSituation2 = "-";
                }
                objs.Add(obj);
            }
            return objs.OrderBy(c=>c.NumOfConsignment).OrderBy(c=>c.NumOfIngot).ToList();
        }


        public IngotModel Get(Guid id)
        {
            var collection = context.Ingots.Select(c => c).ToList();
            var item = collection.Where(c => c.IdIngot == id).Select(c => c).SingleOrDefault();
            IngotModel obj = new IngotModel();
            obj.IdIngot = item.IdIngot;
            obj.NumOfConsignment = item.NumConsigment;
            obj.NumOfIngot = item.NumOfIngot;
            obj.DateOfGrowth = (DateTime)item.DateOfGrowth;
            obj.DateLabAnalysis = (DateTime)item.DateLabAnalysis;
            obj.PercDamage = item.PercDamage;
            obj.MaxDiameter = item.MaxDiameter;
            obj.MinDiameter = item.MinDiameter;
            obj.Resistivity = item.Resistivity;
            obj.HallCoefficient = item.HallCoefficient;
            obj.OutputHallCoef = item.HallCoefficient.ToString("E");
            obj.OutputResistivity = item.Resistivity.ToString("E");
            string[] dateGrowth = item.DateOfGrowth.ToString().Split(' ');
            string[] dateLabAnalysis = item.DateLabAnalysis.ToString().Split(' ');
            obj.OutputDateOfGrowth = dateGrowth[0];
            obj.OutputDateOfLabAnalysis = dateLabAnalysis[0];
            var col = context.Ingots.Where(c => c.IdIngot == item.IdIngot).Join(context.Damages, l => l.IdIngot, d => d.IdIngot, (l, d) => d).Select(c => c.IdSituation);

            var code = col.Select(c => c).Where(c => c.StartsWith("1")).ToList();
            if (code.Count() == 1)
                obj.CodOfUnforeseenSituation = code.FirstOrDefault();
            else obj.CodOfUnforeseenSituation = "-";
            var codeCrit = col.Select(c => c).Where(c => c.StartsWith("0")).ToList();
            if (codeCrit.Count == 1)
            {
                obj.CodOfCriticalSituation1 = codeCrit.First();
                obj.CodOfCriticalSituation2 = "-";
            }
            else if (codeCrit.Count == 2)
            {
                obj.CodOfCriticalSituation1 = codeCrit.First();
                obj.CodOfCriticalSituation2 = codeCrit.Last();
            }
            else
            {
                obj.CodOfCriticalSituation1 = "-";
                obj.CodOfCriticalSituation2 = "-";
            }
            return obj;
        }

        public List<IngotModel> Get(int skip, int take)
        {
            return Get().Skip(skip).Take(take).ToList();
        }

        public void Create(IngotModel dataObject)
        {
            Ingot obj = new Ingot();
            obj.IdIngot = dataObject.IdIngot;
            obj.NumConsigment = dataObject.NumOfConsignment;
            obj.NumOfIngot = dataObject.NumOfIngot;
            obj.MaxDiameter = dataObject.MaxDiameter;
            obj.MinDiameter = dataObject.MinDiameter;
            obj.PercDamage = dataObject.PercDamage;
            obj.DateLabAnalysis = dataObject.DateLabAnalysis;
            obj.DateOfGrowth = dataObject.DateOfGrowth;
            obj.Resistivity = dataObject.Resistivity;
            obj.HallCoefficient = dataObject.HallCoefficient;
            context.Ingots.Add(obj);
            context.SaveChanges();
            statisticObj.ChangeStatistic(dataObject, "create");
            }


       
        public void Update(IngotModel dataObject)
        {
            var obj = context.Ingots.Where(c => c.IdIngot == dataObject.IdIngot).Select(c => c).SingleOrDefault();
            obj.NumConsigment = dataObject.NumOfConsignment;
            obj.NumOfIngot = dataObject.NumOfIngot;
            obj.MaxDiameter = dataObject.MaxDiameter;
            obj.MinDiameter = dataObject.MinDiameter;
            obj.PercDamage = dataObject.PercDamage;
            obj.DateLabAnalysis = dataObject.DateLabAnalysis;
            obj.DateOfGrowth = dataObject.DateOfGrowth;
            obj.Resistivity = dataObject.Resistivity;
            obj.HallCoefficient = dataObject.HallCoefficient;
            context.SaveChanges();
            statisticObj.ChangeStatistic(dataObject, "edit");
        }

        public void Delete(IngotModel dataObject)
        {
            var collection = context.Ingots.Select(c => c).ToList();
            var obj = collection.Where(c => c.IdIngot == dataObject.IdIngot).Select(c => c).SingleOrDefault();
            context.Ingots.Remove(obj);
            context.SaveChanges();
            var ingots = collection.Where(c => c.NumConsigment == dataObject.NumOfConsignment).Select(c => c);
            var item = context.ConsigmentStatistics.Where(c => c.NumConsigment == dataObject.NumOfConsignment).FirstOrDefault();
            if (ingots.Count() == 0) context.ConsigmentStatistics.Remove(item);
            context.SaveChanges();
        }

        public List<IngotModel> Search(string str)
        {
            var col = Get().Where(c => c.CodOfCriticalSituation1.Contains(str) || c.CodOfCriticalSituation2.Contains(str) ||
                c.CodOfUnforeseenSituation.Contains(str) || c.MaxDiameter.ToString().Contains(str) || c.MinDiameter.ToString().Contains(str) || 
                c.OutputDateOfGrowth.Contains(str) || c.OutputDateOfLabAnalysis.Contains(str) || c.OutputHallCoef.Contains(str) || c.OutputResistivity.Contains(str) ||
                c.NumOfConsignment.ToString().Contains(str) || c.NumOfIngot.ToString().Contains(str)
                || c.PercDamage.ToString().Contains(str)).Select(c=>c);
            return col.ToList();
        }
    }
}
