﻿using Monitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.DataVisualization.Charting;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for ChartByAvgMaterial.xaml
    /// </summary>
    public partial class ChartByAvgMaterial
    {
        StatisticService statistic = new StatisticService();
        public void GetGraph(DateTime date)
        {
            List<PointMaterial> materialByShifts = new List<PointMaterial>();
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var collection = Factory.CreateIngotService().Get().Where(c => c.DateOfGrowth.Month == date.Month).Select(c => c);
                var dates = collection.Select(c => c.DateOfGrowth).Distinct();
                if (collection.Count() < 5) MessageBox.Show("Недостатньо даних для побудови графіка!");
                else
                {
                    foreach (var item in dates)
                    {
                        var productsByCurDate = collection.Where(c => c.DateOfGrowth == item).Select(c => c).ToList();
                        PointMaterial point = new PointMaterial
                        {
                            Shift = item,
                            CountMaterial = Math.Round(100-100 * statistic.PartDamaged(productsByCurDate) / productsByCurDate.Count(), 2)
                        };
                        materialByShifts.Add(point);
                    }
                    var avgDamage = Math.Round(100-100 * statistic.PartDamaged(collection.ToList()) / collection.Count(), 2);
                    List<PointMaterial> avgDamaged = new List<PointMaterial>()
                   {
                       
                       new PointMaterial{ Shift=dates.Min(), CountMaterial=avgDamage},
                       new PointMaterial { Shift=dates.Max(), CountMaterial=avgDamage}
                   };
                    lineAvg.ItemsSource = avgDamaged;
                    lineMaterial.ItemsSource = materialByShifts;
                }
            }
        }
        public ChartByAvgMaterial()
        {
            InitializeComponent();
            GetGraph(Convert.ToDateTime(new DateTime(2015, 6, 14, 6, 32, 0)));
        }

        private void BuildGraph(object sender, RoutedEventArgs e)
        {
            DateTime date;
            try
            {
                date = Convert.ToDateTime(datePickerMaterial.Text);
                GetGraph(date);
            }
           catch (Exception) { } 
        }

    }
    public class PointMaterial
    {
        public double CountMaterial { get; set; }
        public DateTime Shift { get; set; }

    }
}