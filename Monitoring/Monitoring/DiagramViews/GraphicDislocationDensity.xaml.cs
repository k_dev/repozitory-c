﻿using Monitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for GraphicDislocationDensity.xaml
    /// </summary>
    public partial class GraphicDislocationDensity
    {


        public void GetGraph(DateTime date)
        {
            List<PointDisloc> bottomPlateDensity = new List<PointDisloc>();
            List<PointDisloc> topPlateDensity = new List<PointDisloc>();
            //List<PointDisloc> middlePlateDensity = new List<PointDisloc>();
            List<PointDisloc> averageDislocationDensity = new List<PointDisloc>();
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var collection = context.Ingots.Join(context.Plates, i => i.IdIngot, p => p.IdIngot, (i, p) => new { i.DateOfGrowth, p.NumPlate, p.DislocationDensity }).Where(c => c.DateOfGrowth.Month == date.Month);
                if (collection.Count() < 5) MessageBox.Show("Недостатньо даних для побудови графіка!");
                else
                {
                    var dates = collection.Select(c => c.DateOfGrowth).Distinct().OrderBy(c=>c);
                    //var avgDens=collection.Select(c => c.DislocationDensity).Average();
                    //PointDisloc point1 = new PointDisloc { DislocationDensity = avgDens, Shift = collection.Select(c => c.DateOfGrowth).Min() };
                    //PointDisloc point2 = new PointDisloc { DislocationDensity = avgDens, Shift = collection.Select(c => c.DateOfGrowth).Max() };
                    //averageDislocationDensity.Add(point1);
                    //averageDislocationDensity.Add(point2);
                    foreach (var item in dates)
                    {
                        
              PointDisloc minPoint = new PointDisloc { Shift = item, DislocationDensity = collection.Where(c=>c.DateOfGrowth==item).Select(c=>c.DislocationDensity).Min() };
              bottomPlateDensity.Add(minPoint);
              PointDisloc maxPoint = new PointDisloc { Shift = item, DislocationDensity = collection.Where(c => c.DateOfGrowth == item).Select(c => c.DislocationDensity).Max() };
              topPlateDensity.Add(maxPoint);
              PointDisloc avgPoint = new PointDisloc { Shift = item, DislocationDensity = collection.Where(c => c.DateOfGrowth == item).Select(c => c.DislocationDensity).Average() };
              averageDislocationDensity.Add(avgPoint);
                        //if (item.NumPlate == 1) bottomPlateDensity.Add(point);
                        ////if (item.NumPlate == 2) middlePlateDensity.Add(point);
                        //if (item.NumPlate == 3) topPlateDensity.Add(point);
                    }

                }
            }

            avgDisl.ItemsSource = averageDislocationDensity;
            topDislocation.ItemsSource = topPlateDensity;
            bottomDislocation.ItemsSource = bottomPlateDensity;
        }


        public GraphicDislocationDensity()
        {
            InitializeComponent();
            GetGraph(Convert.ToDateTime(new DateTime(2015, 6, 14, 6, 32, 0)));
        }
        public class PointDisloc
        {
            public double DislocationDensity { get; set; }
            public DateTime Shift { get; set; }

        }

        private void BuildGraph(object sender, RoutedEventArgs e)
        {
            DateTime date;
            try
            {
                date = Convert.ToDateTime(datePickerDisloc.Text);
                GetGraph(date);
            }
            catch (Exception) { }

        }
    }
 
}