﻿using Monitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for GraphicByDamage.xaml
    /// </summary>
    /// 

    public partial class GraphicByDamage
    {
        StatisticService statistic = new StatisticService();

        public GraphicByDamage()
        {
            InitializeComponent();
            GetGraph(Convert.ToDateTime(new DateTime(2015, 6, 14, 6, 32, 0)));

        }
        public void GetGraph(DateTime date)
        {
            List<PointDamaged> damageByShifts = new List<PointDamaged>();
            using (MonitoringDataEntities context = new MonitoringDataEntities())
            {
                var collection = Factory.CreateIngotService().Get().Where(c => c.DateOfGrowth.Month == date.Month).Select(c => c);
                var dates = collection.Select(c => c.DateOfGrowth).Distinct();
                if (collection.Count() < 5) MessageBox.Show("Недостатньо даних для побудови графіка!");
                else
                {
                    foreach (var item in dates)
                    {
                        var productsByCurDate=collection.Where(c=>c.DateOfGrowth==item).Select(c=>c).ToList();
                        PointDamaged point = new PointDamaged { Shift=item,
                        PercDamaged=Math.Round(100*statistic.PartDamaged(productsByCurDate)/productsByCurDate.Count(),2)
                        };
                        damageByShifts.Add(point);
                    }
                    var avgDamage = Math.Round(100 * statistic.PartDamaged(collection.ToList()) / collection.Count(), 2);
                     List<PointDamaged> avgDamaged = new List<PointDamaged>()
                   {
                       
                       new PointDamaged{ Shift=dates.Min(), PercDamaged=avgDamage},
                       new PointDamaged { Shift=dates.Max(), PercDamaged=avgDamage}
                   };
                lineAvg.ItemsSource = avgDamaged;
                lineDamaged.ItemsSource = damageByShifts;
                }
            }
        }
        private void BuildGraph(object sender, RoutedEventArgs e)
        {
                    DateTime date;
                    try
                    {
                        date = Convert.ToDateTime(datePickerDamage.Text);
                        GetGraph(date);
                    }
                    catch (Exception) { } 
                }
        }
        public class PointDamaged
        {
            public double PercDamaged { get; set; }
            public DateTime Shift { get; set; }

        }
}