﻿using Monitoring.Models;
using Monitoring.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for PlateView.xaml
    /// </summary>
    public partial class PlateView
    {
        private string _action;
        private Guid _id;
        PlateService plateService = new PlateService();
        StatisticService statisticService = new StatisticService();
        public DataGrid dataGridPlate { set; get; }
        public DataGrid dataGridStatistic { set; get; }
        public PlateView(string action, PlateModel obj)
        {
            _action = action;
            InitializeComponent();
            int[] arr = new int[] { 1, 2, 3 };
            var ingots = Factory.CreateIngotService().Get().ToList();
            numConsigment.ItemsSource = ingots.Select(c => c.NumOfConsignment).Distinct().OrderBy(c=>c);
            numIngot.ItemsSource = ingots.Select(c => c.NumOfIngot).Distinct();
            numPlate.ItemsSource = arr;
            if (_action == "Edit")
            {
                numConsigment.SelectedValue = obj.NumOfConsigment;
                numIngot.SelectedValue = obj.NumIngot;
                numPlate.SelectedValue = obj.NumPlate;
                textDislocation.Text = obj.DislocationDensity.ToString();
                _id = obj.IdPlate;
            }
        }

        private void Save_Plate(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(textDislocation.Text) || String.IsNullOrWhiteSpace(textDislocation.Text) ||
               (numConsigment.SelectedValue == null) || (numPlate.SelectedValue == null) || (!Regex.IsMatch(textDislocation.Text, @"[\d{1},\d{2}E+d]$"))
                || (numIngot.SelectedValue == null))
                MessageBox.Show("Некоректне заповнення!");
            else
            {

                PlateModel plate = new PlateModel();
                try
                {
                    plate.DislocationDensity = Convert.ToDouble(textDislocation.Text);
                    plate.NumPlate = Convert.ToInt32(numPlate.SelectedValue.ToString());
                    var id = plateService.GetIdOfIngot(Convert.ToInt32(numConsigment.SelectedValue.ToString()), Convert.ToInt32(numIngot.SelectedValue.ToString()));
         
                if (id != Guid.Empty)
                {
                    plate.IdIngot = id;
                    if (_action == "Create")
                    {
                        plate.IdPlate = Guid.NewGuid();
                        plateService.Create(plate);
                    }
                    if (_action == "Edit")
                    {
                        plate.IdPlate = _id;
                        plateService.Update(plate);
                    }
                    dataGridPlate.ItemsSource = plateService.Get().OrderBy(c=>c.DateOfGrowth);
                    dataGridStatistic.ItemsSource = statisticService.GetStatistic();                         
                    this.Close();
                }

                else MessageBox.Show("Злитку із заданим номером не існує!");
                }
                catch (Exception) { };
            }
        }
    }
}