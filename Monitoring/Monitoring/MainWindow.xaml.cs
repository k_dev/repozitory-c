﻿using Monitoring.Models;
using Monitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    ///
    /// </summary>
    /// ///Программа
    public partial class MainWindow
    {
        StatisticService statistic = new StatisticService();

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                var getIngotData = Factory.CreateIngotService().Get();
                if (getIngotData.Count != 0)
                    dataGridAnalysis.ItemsSource = getIngotData.OrderBy(c => c.DateOfGrowth);
                var stat = statistic.GetStatistic();
                if (stat.Count != 0)
                    dataGridStatistic.ItemsSource = stat;
                var situations = Factory.CreateUnforeseenSituationService().Get();
                if (situations.Count != 0)
                    dataGridSituations.ItemsSource = situations;
                var plates = Factory.CreatePlateService().Get();
                if (plates.Count != 0)
                    dataGridPlate.ItemsSource = plates.OrderBy(c=>c.DateOfGrowth);
            }
            catch (Exception) { }
        }


        private void AddCriticalSituation(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dataGridAnalysis.SelectedItems.Count == 1)
                {
                    var item = (IngotModel)dataGridAnalysis.SelectedItem;
                    if (item.IdIngot != Guid.Empty)
                    {
                        AddSituation add = new AddSituation(item.IdIngot);
                        add.dataGridAnalysis = this.dataGridAnalysis;
                        add.dataGridStatistic = this.dataGridStatistic;
                        add.ShowDialog();
                    }
                    else MessageBox.Show("Виберіть запис!");
                }
                else MessageBox.Show("Виберіть запис!");
            }
            catch (Exception) { };

        }



        private void EditLabAnalysis(object sender, RoutedEventArgs e)
        {
            if (dataGridAnalysis.SelectedItems.Count == 1)
            {
                try
                {
                    var item = (IngotModel)dataGridAnalysis.SelectedItem;
                    if (item.IdIngot != Guid.Empty)
                    {
                        AddLabAnalysis obj = new AddLabAnalysis("Edit", item);
                        obj.dataGridAnalysis = this.dataGridAnalysis;
                        obj.dataGridStatistic = this.dataGridStatistic;
                        obj.ShowDialog();
                    }
                }
                catch (Exception) { }
            }
            else MessageBox.Show("Оберіть елемент для редагування!");
        }

        private void AddLabAnalysisRecord(object sender, RoutedEventArgs e)
        {
            try
            {
                IngotModel obj = new IngotModel();
                AddLabAnalysis addLabAnalysisObj = new AddLabAnalysis("Create", obj);
                addLabAnalysisObj.dataGridAnalysis = this.dataGridAnalysis;
                addLabAnalysisObj.dataGridStatistic = this.dataGridStatistic;
                addLabAnalysisObj.ShowDialog();
            }
            catch (Exception) { }
        }
        private void AddPlateRecord(object sender, RoutedEventArgs e)
        {
            try
            {
                PlateModel obj = new PlateModel();
                PlateView plateViewObj = new PlateView("Create", obj);
                plateViewObj.dataGridPlate = this.dataGridPlate;
                plateViewObj.dataGridStatistic = this.dataGridStatistic;
                plateViewObj.ShowDialog();
            }
            catch (Exception) { }
        }

        private void DeleteLabAnalysis(object sender, RoutedEventArgs e)
        {
            if (dataGridAnalysis.SelectedItems.Count == 1)
            {
                try
                {
                    var item = (IngotModel)dataGridAnalysis.SelectedItem;
                    if (item.IdIngot != Guid.Empty)
                    {
                        Factory.CreateIngotService().Delete(item);
                        var elements = Factory.CreateIngotService().Get();
                        if (elements.Count != 0)
                            dataGridAnalysis.ItemsSource = elements.OrderBy(c => c.DateOfGrowth);
                        else
                        {
                            List<IngotModel> list = new List<IngotModel>();
                            dataGridAnalysis.ItemsSource = list;
                        }
                        statistic.ChangeStatistic(item, "delete");
                        dataGridStatistic.ItemsSource = statistic.GetStatistic();
                    }
                }
                catch (Exception) { }
            }
            else MessageBox.Show("Оберіть елемент для видалення!");
        }

        private void Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void AddUnforeseenSituation(object sender, RoutedEventArgs e)
        {
            try
            {
                UnforeseenSituationModel obj = new UnforeseenSituationModel();
                AddUnforeseenSituation situation = new AddUnforeseenSituation("Create", obj);
                situation.dataGridSituations = this.dataGridSituations;
                situation.ShowDialog();
            }
            catch (Exception) { }
        }

        private void EditUnforeseenSituation(object sender, RoutedEventArgs e)
        {
            if (dataGridSituations.SelectedItems.Count == 1)
            {
                try
                {
                    var item = (UnforeseenSituationModel)dataGridSituations.SelectedItem;
                    if (item != null)
                    {
                        AddUnforeseenSituation obj = new AddUnforeseenSituation("Edit", item);
                        obj.dataGridSituations = this.dataGridSituations;
                        obj.ShowDialog();
                    }
                }
                catch (Exception) { }
            }
            else MessageBox.Show("Оберіть елемент для редагування!");
        }

        private void DeleteSituation(object sender, RoutedEventArgs e)
        {
            if (dataGridSituations.SelectedItems.Count == 1)
            {
                try
                {
                    var item = (UnforeseenSituationModel)dataGridSituations.SelectedItem;
                    if (!String.IsNullOrEmpty(item.IdSituation))
                    {
                        Factory.CreateUnforeseenSituationService().Delete(item);
                        var elements = Factory.CreateUnforeseenSituationService().Get();
                        if (elements.Count != 0)
                            dataGridSituations.ItemsSource = elements;
                        dataGridAnalysis.ItemsSource = Factory.CreateIngotService().Get();
                        dataGridStatistic.ItemsSource = statistic.GetStatistic();
                    }
                }
                catch (Exception) { }
            }
            else MessageBox.Show("Оберіть елемент для видалення!");
        }


        private void GraphicDensity(object sender, RoutedEventArgs e)
        {
            try
            {
                GraphicDislocationDensity wnd = new GraphicDislocationDensity();
                wnd.ShowDialog();
            }
            catch (Exception) { }
        }

        private void GraphicDefects(object sender, RoutedEventArgs e)
        {
            try
            {
                GraphicByDamage wnd = new GraphicByDamage();
                wnd.ShowDialog();
            }
            catch (Exception) { }
        }
        private void GraphicAvgMaterial(object sender, RoutedEventArgs e)
        {
            try
            {
                ChartByAvgMaterial wnd = new ChartByAvgMaterial();
                wnd.ShowDialog();
            }
            catch (Exception) { }
        }

        private void SearchLabAnalysis(object sender, RoutedEventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(searchLabAnalysis.Text) || String.IsNullOrWhiteSpace(searchLabAnalysis.Text))
                    dataGridAnalysis.ItemsSource = Factory.CreateIngotService().Get();
                else
                {
                    var elements = Factory.CreateIngotService().Search(searchLabAnalysis.Text);
                    if (elements.Count != 0)
                        dataGridAnalysis.ItemsSource = elements.OrderBy(c => c.DateOfGrowth);
                }
            }
            catch (Exception) { }
        }

        private void SearchSituation(object sender, RoutedEventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(searchSituation.Text) || String.IsNullOrWhiteSpace(searchSituation.Text))
                    dataGridSituations.ItemsSource = Factory.CreateUnforeseenSituationService().Get();
                else
                {
                    var elements = Factory.CreateUnforeseenSituationService().Search(searchSituation.Text);
                    if (elements.Count != 0)
                        dataGridSituations.ItemsSource = elements;
                }
            }
            catch (Exception) { }
        }

        private void SearchStatistic(object sender, RoutedEventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(searchStatistic.Text) || String.IsNullOrWhiteSpace(searchStatistic.Text))
                    dataGridStatistic.ItemsSource = statistic.GetStatistic();
                else
                {
                    var elements = statistic.Search(searchStatistic.Text);
                    if (elements.Count != 0)
                        dataGridStatistic.ItemsSource = elements;
                }
            }
            catch (Exception) { }
        }

        private void DeletePlate(object sender, RoutedEventArgs e)
        {
            if (dataGridPlate.SelectedItems.Count == 1)
            {
                try
                {
                    var item = (PlateModel)dataGridPlate.SelectedItem;
                    if (item.IdPlate != Guid.Empty)
                    {
                        Factory.CreatePlateService().Delete(item);
                        var elements = Factory.CreatePlateService().Get();
                        if (elements.Count != 0)
                            dataGridPlate.ItemsSource = elements.OrderBy(c => c.DateOfGrowth);
                        else
                        {
                            List<PlateModel> list = new List<PlateModel>();
                            dataGridAnalysis.ItemsSource = list;
                        }
                        dataGridStatistic.ItemsSource = statistic.GetStatistic();
                    }
                }
                catch (Exception) { }
            }
            else MessageBox.Show("Оберіть елемент для видалення!");
        }

        private void EditPlate(object sender, RoutedEventArgs e)
        {
            if (dataGridPlate.SelectedItems.Count == 1)
            {
                try
                {
                    var item = (PlateModel)dataGridPlate.SelectedItem;
                    if (item != null)
                    {
                        PlateView obj = new PlateView("Edit", item);
                        obj.dataGridPlate = this.dataGridPlate;
                        obj.dataGridStatistic = this.dataGridStatistic;
                        obj.ShowDialog();
                    }
                }
                catch (Exception) { }
            }
            else MessageBox.Show("Оберіть елемент для редагування!");
        }

        private void SearchPlate(object sender, RoutedEventArgs e)
        {
            try
            {
                if (String.IsNullOrWhiteSpace(searchPlate.Text) || String.IsNullOrWhiteSpace(searchPlate.Text))
                    dataGridPlate.ItemsSource = Factory.CreatePlateService().Get();
                else
                {
                    var elements = Factory.CreatePlateService().Search(searchPlate.Text);
                    if (elements.Count != 0)
                        dataGridPlate.ItemsSource = elements.OrderBy(c => c.DateOfGrowth);
                }
            }
            catch (Exception) { }
        }
    }  
}
