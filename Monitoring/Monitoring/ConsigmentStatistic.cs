//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Monitoring
{
    using System;
    using System.Collections.Generic;
    
    public partial class ConsigmentStatistic
    {
        public ConsigmentStatistic()
        {
            this.Ingots = new HashSet<Ingot>();
        }
    
        public int NumConsigment { get; set; }
        public Nullable<double> AvgDislocDensity { get; set; }
        public Nullable<double> AvgResistivity { get; set; }
        public Nullable<double> AvgHallKoef { get; set; }
        public Nullable<double> Damage { get; set; }
    
        public virtual ICollection<Ingot> Ingots { get; set; }
    }
}
