﻿using Monitoring.Models;
using Monitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for AddSituation.xaml
    /// </summary>
    public partial class AddSituation 
    {
        private Guid _id;
        public DataGrid dataGridAnalysis { get; set; }
        public DataGrid dataGridStatistic { get; set; }
        StatisticService statistic = new StatisticService();
        public AddSituation(Guid id)
        {
            InitializeComponent();
            _id = id;
            comboCritical.ItemsSource = Factory.CreateUnforeseenSituationService().Get().Select(c => c.IdSituation);
        }

        private void SaveSituation(object sender, RoutedEventArgs e)
        {
            if ((comboCritical.SelectedValue == null)) MessageBox.Show("Поля обов'язкові для заповнення!");
            else
            {
                DamageModel obj = new DamageModel();
                obj.IdDamage = Guid.NewGuid();
                obj.IdIngot = _id;
                obj.IdSituation = comboCritical.SelectedValue.ToString();
                try
                {
                    Factory.CreateDamageService().Create(obj);
                }
                catch (Exception) { MessageBox.Show("Помилка! Ситуація вже існує або перевищено допустиму кількість ситуацій."); }
                var elements = Factory.CreateIngotService().Get();
                if (elements.Count != 0)
                    dataGridAnalysis.ItemsSource = elements.OrderBy(c=>c.DateOfGrowth);
                dataGridStatistic.ItemsSource = statistic.GetStatistic();
                this.Close();
            }
        }
    }
}
