﻿using Monitoring.Services;
using Monitoring.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for AddLabAnalysis.xaml
    /// </summary>
    public partial class AddLabAnalysis 
    {
        public DataGrid dataGridAnalysis { get; set; }
        public DataGrid dataGridStatistic { get; set; }
        private string _action;
        private Guid id;
        private String datePickerDate;
        StatisticService statistic = new StatisticService();
        IngotModel _item;
        public AddLabAnalysis(string action, IngotModel item)
        {
            InitializeComponent();
            _item = item;
            _action = action;
            MonitoringDataEntities context = new MonitoringDataEntities();
            var col = context.ConsigmentStatistics.Select(c => c.NumConsigment);
            if (col.Count() != 0)
            {
                comboNumConsigment.ItemsSource = col.ToList();
            }
            if(_action=="Edit")
            {
                comboNumConsigment.Text = item.NumOfConsignment.ToString();
                textIngot.Text = item.NumOfIngot.ToString();
                textMinDiameter.Text = item.MinDiameter.ToString();
                textMaxDiameter.Text = item.MaxDiameter.ToString();
                dateLabAnalysis.Text = item.OutputDateOfLabAnalysis;
                dateOfGrowth.Text = item.OutputDateOfGrowth;
                textHall.Text = item.HallCoefficient.ToString("e");
                textResistivity.Text = item.Resistivity.ToString("e");
                textPercDamage.Text = item.PercDamage.ToString();
                id = item.IdIngot;
            }
   
        }

        private void Save(object sender, RoutedEventArgs e)
        {

            if ((comboNumConsigment.SelectedValue==null) || 
                String.IsNullOrEmpty(dateLabAnalysis.Text) || String.IsNullOrWhiteSpace(dateLabAnalysis.Text) ||
                String.IsNullOrEmpty(textIngot.Text) || String.IsNullOrWhiteSpace(textIngot.Text) ||
                String.IsNullOrEmpty(dateOfGrowth.Text) || String.IsNullOrWhiteSpace(dateOfGrowth.Text) ||
                 String.IsNullOrEmpty(textMaxDiameter.Text) || String.IsNullOrWhiteSpace(textMaxDiameter.Text)||
                String.IsNullOrEmpty(textMinDiameter.Text) || String.IsNullOrWhiteSpace(textMinDiameter.Text) || String.IsNullOrWhiteSpace(textResistivity.Text)
                || String.IsNullOrWhiteSpace(textHall.Text) || String.IsNullOrWhiteSpace(textPercDamage.Text))
                MessageBox.Show("Поле обов'язкове для заповнення!");
            else if (!Regex.IsMatch(textIngot.Text, @"^[\s\d]*$") ||  !Regex.IsMatch(textMinDiameter.Text, @"^[\s\d,\d{2}]*$")||
                !Regex.IsMatch(textPercDamage.Text, @"^[\s\d,\d{2}]*$") || !Regex.IsMatch(textMaxDiameter.Text, @"^[\s\d,\d{2}]*$")
                || !Regex.IsMatch(textHall.Text, @"[\d{1},\d{2}E+d]$") || !Regex.IsMatch(textResistivity.Text, @"[\d{1},\d{2}E+d]$"))
                MessageBox.Show("Некоректне заповнення!");
                
            else
            {
                if (_action == "Create")
                {
                    try
                    {
                        IngotModel obj = new IngotModel();
                        obj.IdIngot = Guid.NewGuid();
                        obj.NumOfConsignment = Convert.ToInt32(comboNumConsigment.SelectedValue);
                        obj.NumOfIngot = Convert.ToInt32(textIngot.Text);
                        obj.MinDiameter = Convert.ToDouble(textMinDiameter.Text);
                        obj.MaxDiameter = Convert.ToDouble(textMaxDiameter.Text);
                        obj.PercDamage = Convert.ToDouble(textPercDamage.Text);
                        obj.HallCoefficient = Convert.ToDouble(textHall.Text);
                        obj.Resistivity = Convert.ToDouble(textResistivity.Text);
                        obj.CodOfCriticalSituation1 = "-";
                        obj.CodOfCriticalSituation2 = "-";
                        obj.CodOfUnforeseenSituation = "-";

                        DateTime dateGrowth = new DateTime();
                        DateTime dateLabAn=new DateTime();
                        DateTime.TryParse(dateOfGrowth.Text, out dateGrowth);
                        obj.DateOfGrowth = dateGrowth;
                        DateTime.TryParse(dateLabAnalysis.Text, out dateLabAn);
                        obj.DateLabAnalysis = dateLabAn;
                            Factory.CreateIngotService().Create(obj);                      
                        var elements = Factory.CreateIngotService().Get();
                        if (elements.Count != 0)
                            dataGridAnalysis.ItemsSource = elements.OrderBy(c => c.DateOfGrowth);
                        dataGridStatistic.ItemsSource = statistic.GetStatistic();
                       
                    }
                    catch (Exception) { }
                    this.Close();
                }
                else {
                    try
                    {
                        IngotModel obj = new IngotModel();
                        obj.IdIngot = id;
                        obj.NumOfConsignment = Convert.ToInt32(comboNumConsigment.SelectedValue);
                        obj.NumOfIngot = Convert.ToInt32(textIngot.Text);
                        obj.MinDiameter = Convert.ToDouble(textMinDiameter.Text);
                        obj.MaxDiameter = Convert.ToDouble(textMaxDiameter.Text);
                        obj.PercDamage = Convert.ToDouble(textPercDamage.Text);
                        obj.DateOfGrowth = Convert.ToDateTime(dateOfGrowth.Text);
                        obj.DateLabAnalysis = Convert.ToDateTime(dateLabAnalysis.Text);
                        obj.HallCoefficient = Convert.ToDouble(textHall.Text);
                        obj.Resistivity = Convert.ToDouble(textResistivity.Text);
                        obj.CodOfCriticalSituation1 = _item.CodOfCriticalSituation1;
                        obj.CodOfCriticalSituation2 = _item.CodOfCriticalSituation2;
                        obj.CodOfUnforeseenSituation = _item.CodOfUnforeseenSituation;
                        Factory.CreateIngotService().Update(obj);
                        var elements = Factory.CreateIngotService().Get();
                        if (elements.Count != 0)
                            dataGridAnalysis.ItemsSource = elements.OrderBy(c => c.DateOfGrowth);
                        dataGridStatistic.ItemsSource = statistic.GetStatistic();
                    }
                    catch(Exception){}
                    this.Close();
                
                }
            }
        }

        private void Add_Consigment(object sender, RoutedEventArgs e)
        {
            AddConsigment obj = new AddConsigment();
            obj.comboNumConsigment = this.comboNumConsigment;
            obj.ShowDialog();
        }
    }
}
