﻿using Monitoring.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring
{
    /// <summary>
    /// Interaction logic for AddUnforeseenSituation.xaml
    /// </summary>
    public partial class AddUnforeseenSituation 
    {
        public DataGrid dataGridSituations { set; get; }
        private string _action;
        private string _id;
        Boolean exec=false;
        public AddUnforeseenSituation(string action, UnforeseenSituationModel obj)
        {
            InitializeComponent();
            _action = action;
            if(_action=="Edit")
            {
                _id = obj.IdSituation; 
                textDescription.Text = obj.Description;
                textCauses.Text = obj.Causes;
                textCod.Text = obj.IdSituation;
            }
        }

        private void Save(object sender, RoutedEventArgs e)
        {
            if (String.IsNullOrEmpty(textCod.Text) || String.IsNullOrWhiteSpace(textCauses.Text) ||
               String.IsNullOrEmpty(textDescription.Text) || 
               String.IsNullOrWhiteSpace(textCod.Text) || String.IsNullOrWhiteSpace(textCauses.Text) ||
               String.IsNullOrWhiteSpace(textDescription.Text) || String.IsNullOrWhiteSpace(textDescription.Text))
                MessageBox.Show("Поле обов'язкове для заповнення!");
            else if (!textCod.Text.StartsWith("1") && !textCod.Text.StartsWith("0")) MessageBox.Show("Код ситуації повинен починатись з 0 або 1!");
            else if (!Regex.IsMatch(textCod.Text, @"^[\s\d]*$")) MessageBox.Show("Некоректне заповнення!");
            else
            {
               
                    UnforeseenSituationModel situation = new UnforeseenSituationModel();
                    situation.IdSituation = textCod.Text;
                    situation.Causes = textCauses.Text;
                    situation.Description = textDescription.Text;
                    if (_action == "Create")
                    {
                        var col = Factory.CreateUnforeseenSituationService().Get().Where(c => c.IdSituation == textCod.Text);
                        if (col.Count() == 0)
                        {
                            Factory.CreateUnforeseenSituationService().Create(situation);
                            exec = true;
                        }
                        else MessageBox.Show("Ситуация с таким кодом уже существует!");
                    }
                else
                {
                    if (textCod.Text != _id) // if id has been changed
                    {
                        try
                        {
                            var col = Factory.CreateUnforeseenSituationService().Get().Where(c => c.IdSituation == textCod.Text);
                            if (col.Count() == 0)
                            {
                                situation.IdSituation = textCod.Text;
                                Factory.CreateUnforeseenSituationService().Delete(Factory.CreateUnforeseenSituationService().Get().Where(c => c.IdSituation == _id).SingleOrDefault());
                                Factory.CreateUnforeseenSituationService().Create(situation);
                                exec = true;
                            }
                            else { MessageBox.Show("Ситуация с таким кодом уже существует!");  }
                        } catch(Exception){}
                    }
                    else
                    Factory.CreateUnforeseenSituationService().Update(situation);
                }
                    dataGridSituations.ItemsSource = Factory.CreateUnforeseenSituationService().Get();
                
                   if(exec) this.Close();
            }
        }

    }
}
