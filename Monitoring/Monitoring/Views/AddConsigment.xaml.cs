﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Monitoring.Views
{
    /// <summary>
    /// Interaction logic for AddConsigment.xaml
    /// </summary>
    public partial class AddConsigment 
    {
        public ComboBox comboNumConsigment { set; get; }
        public AddConsigment()
        {
            InitializeComponent();
        }

        private void Save(object sender, RoutedEventArgs e)
        {
             if (String.IsNullOrEmpty(textNumConsigment.Text)|| String.IsNullOrWhiteSpace(textNumConsigment.Text))
             MessageBox.Show("Поле обов'язкове для заповнення!");
             else if (!Regex.IsMatch(textNumConsigment.Text, @"^[\s\d]*$")) MessageBox.Show("Некоректне заповнення!"); else
             {
            MonitoringDataEntities context = new MonitoringDataEntities();
                 var numConsigment=Convert.ToInt32(textNumConsigment.Text);
                 var col=context.ConsigmentStatistics.Where(c=>c.NumConsigment==numConsigment).Select(c=>c).ToList();
                 if (col.Count() != 0) MessageBox.Show("Партія з таким номером вже існує!");
                 else
                 {
                     ConsigmentStatistic obj = new ConsigmentStatistic();
                     obj.NumConsigment = Convert.ToInt32(textNumConsigment.Text);
                     context.ConsigmentStatistics.Add(obj);
                     context.SaveChanges();
                     var collection = context.ConsigmentStatistics.Select(c => c.NumConsigment);
                     if (collection.Count() != 0)
                     {
                         comboNumConsigment.ItemsSource = collection.ToList();
                     }
                     this.Close();
                 }
             }
             
        }
    }
}
